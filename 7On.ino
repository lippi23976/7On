
// define pin variables
int reset = A7; // reset the game
int btn1 = A0;  // button 1
int btn2 = A1;  // button 2
int btn3 = A2;  // button 3
int btn4 = A3;  // button 4
int btn5 = A4;  // button 5
int btn6 = A5;  // button 6
int btn7 = A6;  // button 7
int led1 = 40;  // LED 1
int led2 = 42;  // LED 2
int led3 = 44;  // LED 3
int led4 = 46;  // LED 4
int led5 = 48;  // LED 5
int led6 = 50;  // LED 6
int led7 = 52;  // LED 7

// define buffer variables
bool btn1state = false;
bool btn2state = false;
bool btn3state = false;
bool btn4state = false;
bool btn5state = false;
bool btn6state = false;
bool btn7state = false;
bool waitforbtnrelease = false;

// the setup method will run once on startup
void setup()
{
  // configure input pins
  pinMode(btn1, INPUT_PULLUP);
  pinMode(btn2, INPUT_PULLUP);
  pinMode(btn3, INPUT_PULLUP);
  pinMode(btn4, INPUT_PULLUP);
  pinMode(btn5, INPUT_PULLUP);
  pinMode(btn5, INPUT_PULLUP);
  pinMode(btn6, INPUT_PULLUP);
  pinMode(btn7, INPUT_PULLUP);
  pinMode(reset, INPUT_PULLUP);

  // configure output pins
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
  pinMode(led5, OUTPUT);
  pinMode(led6, OUTPUT);
  pinMode(led7, OUTPUT);
  
  // switch off all LED
  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
  digitalWrite(led3, LOW);
  digitalWrite(led4, LOW);
  digitalWrite(led5, LOW);
  digitalWrite(led6, LOW);
  digitalWrite(led7, LOW);
  
  // start debug output on serial port
  Serial.begin(9600);
}

// the loop method will be called constantly
void loop()
{
  if (!digitalRead(reset))
  {
    // reset game if reset button is pressed
    digitalWrite(led1, LOW);
    digitalWrite(led2, LOW);
    digitalWrite(led3, LOW);
    digitalWrite(led4, LOW);
    digitalWrite(led5, LOW);
    digitalWrite(led6, LOW);
    digitalWrite(led7, LOW);
    btn1state = false;
    btn2state = false;
    btn3state = false;
    btn4state = false;
    btn5state = false;
    btn6state = false;
    btn7state = false;
    waitforbtnrelease = false;

    Serial.println("reset");
  }
  else
  {
    // get button state
    // invert value, because LOW = pressed, HIGH = not pressed
    btn1state = !digitalRead(btn1);
    btn2state = !digitalRead(btn2);
    btn3state = !digitalRead(btn3);
    btn4state = !digitalRead(btn4);
    btn5state = !digitalRead(btn5);
    btn6state = !digitalRead(btn6);
    btn7state = !digitalRead(btn7);
    
    if (waitforbtnrelease)
    {
      // a button was pressed,
      // wait until all buttons have been released
      if (!btn1state && !btn2state && !btn3state && !btn4state && !btn5state && !btn6state && !btn7state)
      {
        waitforbtnrelease = false;
        Serial.println("btn release");
      }
    }
    else
    {
      // debounce buttons
      // check inputs again after 10ms for HIGH state
      delay(10);
      if (btn1state && digitalRead(btn1))
      {
        btn1state = false;
      }
      if (btn2state && digitalRead(btn2))
      {
        btn2state = false;
      }
      if (btn3state && digitalRead(btn3))
      {
        btn3state = false;
      }
      if (btn4state && digitalRead(btn4))
      {
        btn4state = false;
      }
      if (btn5state && digitalRead(btn5))
      {
        btn5state = false;
      }
      if (btn6state && digitalRead(btn6))
      {
        btn6state = false;
      }
      if (btn7state && digitalRead(btn7))
      {
        btn7state = false;
      }

      // change LED state
      if (btn1state)
      {
        // wait for button release before we change another state
        waitforbtnrelease = true;

        // toggle LED7, LED1 and LED2
        // works only on AVR chips
        digitalWrite(led7, !digitalRead(led7));
        digitalWrite(led1, !digitalRead(led1));
        digitalWrite(led2, !digitalRead(led2));
  
        Serial.println("btn1");
      }
      else if (btn2state)
      {
        // wait for button release before we change another state
        waitforbtnrelease = true;
  
        // toggle LED1, LED2 and LED3
        // works only on AVR chips
        digitalWrite(led1, !digitalRead(led1));
        digitalWrite(led2, !digitalRead(led2));
        digitalWrite(led3, !digitalRead(led3));
  
        Serial.println("btn2");
      }
      else if (btn3state)
      {
        // wait for button release before we change another state
        waitforbtnrelease = true;
  
        // toggle LED2 and LED4
        // works only on AVR chips
        digitalWrite(led2, !digitalRead(led2));
        digitalWrite(led3, !digitalRead(led3));
        digitalWrite(led4, !digitalRead(led4));
  
        Serial.println("btn3");
      }
      else if (btn4state)
      {
        // wait for button release before we change another state
        waitforbtnrelease = true;
  
        // toggle LED3 and LED5
        // works only on AVR chips
        digitalWrite(led3, !digitalRead(led3));
        digitalWrite(led4, !digitalRead(led4));
        digitalWrite(led5, !digitalRead(led5));
        
        Serial.println("btn4");
      }
      else if (btn5state)
      {
        // wait for button release before we change another state
        waitforbtnrelease = true;
  
        // toggle LED4 and LED6
        // works only on AVR chips
        digitalWrite(led4, !digitalRead(led4));
        digitalWrite(led5, !digitalRead(led5));
        digitalWrite(led6, !digitalRead(led6));
        
        Serial.println("btn5");
      }
      else if (btn6state)
      {
        // wait for button release before we change another state
        waitforbtnrelease = true;
  
        // toggle LED5 and LED7
        // works only on AVR chips
        digitalWrite(led5, !digitalRead(led5));
        digitalWrite(led6, !digitalRead(led6));
        digitalWrite(led7, !digitalRead(led7));
  
        Serial.println("btn6");
      }
      else if (btn7state)
      {
        // wait for button release before we change another state
        waitforbtnrelease = true;
  
        // toggle LED6 and LED1
        // works only on AVR chips
        digitalWrite(led6, !digitalRead(led6));
        digitalWrite(led7, !digitalRead(led7));
        digitalWrite(led1, !digitalRead(led1));
  
        Serial.println("btn7");
      }
    }
  }

  delay(10);
}
